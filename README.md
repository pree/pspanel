﻿Easy to use, multifunction powershell display windows.

WORK IN PROGRESS

Start-Panel		- starts panel listening

Out-Page		- sends pipe output to a page on a panel

Clear-Page		- clears a page on a panel

Set-Page		- sets current page on a panel

![](https://gitlab.com/pree/pspanel/raw/master/PsPanel.gif)
