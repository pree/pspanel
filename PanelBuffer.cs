﻿

namespace PSPanel
{
    using System;
    using System.Collections.Generic;
    using System.Management.Automation.Host;

    internal class PanelBuffer
    {
        public static readonly string DefaultPage = "{75F1B9A5-DA5A-4D0D-AFB8-F0882AD5AB6D}";
        public static readonly string BlankPage = "{44E00A32-3A55-44F6-A527-8D9B775954FF}";

        private static Dictionary<string, BufferCell[,]> library = new Dictionary<string, BufferCell[,]>();

        static private string _page = DefaultPage;


        internal static string CheckPage(string page)
        {
            return string.IsNullOrWhiteSpace(page) ? DefaultPage : page;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        internal static void RemovePage(PSHost host, string page)
        {
            page = CheckPage(page);

            if (library.ContainsKey(page))
                library.Remove(page);

            Restore(host, BlankPage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="row"></param>
        /// <param name="rowData"></param>
        internal static void Store(string page, int row, string rowData)
        {
            page = CheckPage(page);

            var bufferRow = new BufferCell[rowData.Length];

            // copy the string into a rowbuffer
            for (int i = 0; i < rowData.Length; i++)
            {
                bufferRow[i].Character = rowData[i];
                bufferRow[i].BackgroundColor = ConsoleColor.DarkBlue;
                bufferRow[i].ForegroundColor = ConsoleColor.Yellow;
            }
            Store(page, row, bufferRow);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="row"></param>
        /// <param name="rowData"></param>
        internal static void Store(string page, int row, BufferCell[] rowData)
        {
            page = CheckPage(page);

            if (!library.ContainsKey(page))
                library.Add(page, new BufferCell[1000, 200]);

            BufferCell[,] pageBuffer = library[page];

            int inLen = rowData.Length;
            int outLen = pageBuffer.GetLength(1);

            // use whichever is shortest
            var todo = inLen < outLen ? inLen : outLen;

            // copy into pagebuffer
            for (int i = 0; i < todo; i++)
            {
                pageBuffer[row, i] = rowData[i];
            }
        }

        /// <summary>
        /// Restores a page to the UI
        /// </summary>
        /// <param name="host"></param>
        /// <param name="page"></param>
        internal static void Restore(PSHost host, string page)
        {
            page = CheckPage(page);

            var origin = new Coordinates(0, 0);

            if (page == BlankPage)
            {
                // just show a new blank buffer
                host.UI.RawUI.SetBufferContents(origin, host.UI.RawUI.NewBufferCellArray(host.UI.RawUI.WindowSize.Width, host.UI.RawUI.WindowSize.Height, new BufferCell(' ', ConsoleColor.Yellow, ConsoleColor.DarkBlue, BufferCellType.Complete)));
            }
            else if (library.ContainsKey(page))
            {
                _page = page;                
                host.UI.RawUI.SetBufferContents(origin, library[page]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        internal static void Refresh(PSHost host)
        {
            Restore(host, _page);
        }
    }
}
