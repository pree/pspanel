﻿

namespace PSPanel
{
    using ProtoBuf;
    using System;
    using System.IO.Pipes;
    using System.Management.Automation;

    [Cmdlet(VerbsData.Out, "page")]
    public class OutPage : Cmdlet, IDisposable
    {
        [Parameter(Mandatory = false, Position = 1)]
        public string Page { get; set; }

        [Parameter(Mandatory = false, ValueFromPipeline = true, Position = 2)]
        public String Value { get; set; }        

        [Parameter(Mandatory = false, Position = 3)]
        public int BeginRow { get; set; }

        [Parameter(Mandatory = false, Position = 4)]
        public int EndRow { get; set; }

        [Parameter(Mandatory = false, Position = 5)]
        public SwitchParameter AnchorBottom { get; set; }

        [Parameter(Mandatory = false, Position = 6)]
        public SwitchParameter SkipFocus { get; set; }

        [Parameter(Mandatory = false, Position = 7)]
        public string PanelName { get; set; }

        private NamedPipeClientStream pipe;
        private int counter = 0;

        public OutPage()
        {
            BeginRow = 0;
            EndRow = -1;
            AnchorBottom = false;
        }

        protected override void BeginProcessing()
        {
            PanelName = string.IsNullOrWhiteSpace(PanelName) ? StartPanel.DefaultPipe : PanelName;

            pipe = new NamedPipeClientStream(".", PanelName, PipeDirection.InOut);
            pipe.Connect();
            counter = BeginRow;

            // EndRow should be at or after BeginRow
            // if endrow isn't set, make it unlimited
            EndRow = EndRow < BeginRow ? 1000 : EndRow;
        }

        protected override void ProcessRecord()
        {
            if (counter <= EndRow)
            {
                // first send the header
                var header = new Header { CommandType = typeof(OutPanelCommand).FullName, DataType = typeof(OutCommandData).FullName };
                Serializer.SerializeWithLengthPrefix<Header>(pipe, header, PrefixStyle.Fixed32);

                // now send the data
                var data = new OutCommandData() { Value = string.IsNullOrEmpty(Value)?"":Value, Row = counter++, AnchorBottom = AnchorBottom, Page = Page, SkipFocus = SkipFocus };
                Serializer.SerializeWithLengthPrefix<OutCommandData>(pipe, data, PrefixStyle.Fixed32);
            }
        }

        protected override void EndProcessing()
        {
            pipe.Flush();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    pipe.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ClearPanel() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}