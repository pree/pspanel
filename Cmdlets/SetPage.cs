﻿
namespace PSPanel
{
    using ProtoBuf;
    using System;
    using System.IO.Pipes;
    using System.Management.Automation;

    [Cmdlet(VerbsCommon.Set, "page")]
    public class SetPage : Cmdlet, IDisposable
    {
        [Parameter(Mandatory =false, Position = 1)]
        public string Page { get; set; }

        [Parameter(Mandatory = false, Position = 2)]
        public string PanelName { get; set; }

        private NamedPipeClientStream pipe;

        protected override void BeginProcessing()
        {
            PanelName = string.IsNullOrWhiteSpace(PanelName) ? StartPanel.DefaultPipe : PanelName;

            pipe = new NamedPipeClientStream(".", PanelName, PipeDirection.Out);
            pipe.Connect();
        }

        protected override void ProcessRecord()
        {
            // just send the header this time
            var header = new Header { CommandType = typeof(SetPanelCommand).FullName};
            Serializer.SerializeWithLengthPrefix<Header>(pipe, header, PrefixStyle.Fixed32);

            // now send the data
            var data = new SetCommandData() { Page = Page };
            Serializer.SerializeWithLengthPrefix<SetCommandData>(pipe, data, PrefixStyle.Fixed32);
        }

        protected override void EndProcessing()
        {
            pipe.Flush();           
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    pipe.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ClearPanel() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
