﻿

namespace PSPanel
{
    using ProtoBuf;
    using System;
    using System.IO.Pipes;
    using System.Management.Automation;
    using System.Management.Automation.Host;
    using System.Threading;
    using System.Threading.Tasks;

    [Cmdlet(VerbsLifecycle.Start, "panel")]
    public class StartPanel : PSCmdlet
    {
        internal static readonly string DefaultPipe = "{50A41803-3AED-49A7-87F6-D5C3DE1412BF}";

        [Parameter(Mandatory = false, Position = 1)]
        public string PanelName { get; set; }

        protected override void BeginProcessing()
        {
            PanelName = string.IsNullOrWhiteSpace(PanelName) ? StartPanel.DefaultPipe : PanelName;

            // hide the cursor
            Host.UI.RawUI.CursorSize = 0;
        }

        protected override void ProcessRecord()
        {           
            Size oldsize = Host.UI.RawUI.WindowSize;

            // hack to fix buffer resize
            var t = new Timer((o)=> 
            {
                if(Host.UI.RawUI.WindowSize!=oldsize)
                {
                    oldsize = Host.UI.RawUI.WindowSize;
                    PanelBuffer.Refresh(Host);
                }

            }, null, 100, 100);

            // Only exits when the shell is closed
            while (true)
            {
                using (var pipe = new NamedPipeServerStream(PanelName, PipeDirection.InOut, 5, PipeTransmissionMode.Message))
                {
                    // set window to upper left of buffer
                    Host.UI.RawUI.WindowPosition = new Coordinates(0, 0);

                    pipe.WaitForConnection();

                    while (pipe.IsConnected)
                    {
                        
                        // get the command and data types
                        var command = Serializer.DeserializeWithLengthPrefix<Header>(pipe, PrefixStyle.Fixed32);
                        if (command == null)
                            break;

                        // create the type from the type name
                        var cmdType = Type.GetType(command.CommandType);

                        // get an instance of the command
                        var instance = (IPanelCommand)Activator.CreateInstance(cmdType);

                        // invoke the command, passing the pipe for any other data
                        instance.Run(Host, pipe);
                        System.Threading.Thread.Sleep(1);
                    }
                    // it would be nice to process multiple commands, but it appears you can't
                    // wait for a message without BeginRead. Maybe one day. Until then...
                    pipe.Disconnect();
                }
            }
        }

       
    }
}
