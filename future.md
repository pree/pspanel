new-page        -value -tags
add-pagetag     -tag -page
remove-page
append-page
search-library
set-page                    # Sets current page in panel
get-page                    # Gets an entire page
get-line                    # Get a single line from a page
