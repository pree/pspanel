﻿

namespace PSPanel
{
    using ProtoBuf;
    using System.IO.Pipes;
    using System.Management.Automation.Host;

    class ClearPanelCommand : IPanelCommand
    {
        public void Run(PSHost host, NamedPipeServerStream pipe)
        {
            var outData = Serializer.DeserializeWithLengthPrefix<ClearCommandData>(pipe, PrefixStyle.Fixed32);
            if (outData != null)
                PanelBuffer.RemovePage(host, outData.Page);
        }
    }
}
