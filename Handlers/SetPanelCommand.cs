﻿

namespace PSPanel
{
    using ProtoBuf;
    using System.IO.Pipes;
    using System.Management.Automation.Host;

    class SetPanelCommand : IPanelCommand
    {
        public void Run(PSHost host, NamedPipeServerStream pipe)
        {
            var outData = Serializer.DeserializeWithLengthPrefix<SetCommandData>(pipe, PrefixStyle.Fixed32);
            if (outData != null)
            {
                PanelBuffer.Restore(host, outData.Page);
            }
        }
    }
}
