﻿

namespace PSPanel
{
    using ProtoBuf;
    using System;
    using System.IO.Pipes;
    using System.Management.Automation.Host;

    public class OutPanelCommand : IPanelCommand
    {
        public void Run(PSHost host, NamedPipeServerStream pipe)
        {
            var outData = Serializer.DeserializeWithLengthPrefix<OutCommandData>(pipe, PrefixStyle.Fixed32);
            if (outData != null)
                DrawRow(host, outData);
        }

        private void DrawRow(PSHost Host, OutCommandData data)
        {
            int rowPosition;
            
            if (data.AnchorBottom)
                rowPosition = Host.UI.RawUI.WindowSize.Height - data.Row;
            else
                rowPosition = data.Row;

            PanelBuffer.Store(data.Page, rowPosition, data.Value);

            if(!data.SkipFocus)
                PanelBuffer.Restore(Host, data.Page);
        }
    }
}
