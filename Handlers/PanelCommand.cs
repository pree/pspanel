﻿

namespace PSPanel
{
    using ProtoBuf;
    using System;

    [ProtoContract]
    public class Header
    {
        [ProtoMember(1)]
        public string CommandType { get; set; }

        [ProtoMember(2)]
        public string DataType { get; set; }
    }
}
