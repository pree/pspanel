﻿

namespace PSPanel
{
    using System.IO.Pipes;
    using System.Management.Automation.Host;

    public interface IPanelCommand
    {
        void Run(PSHost host, NamedPipeServerStream pipe);
    }
}
