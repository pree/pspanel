﻿

namespace PSPanel
{
    using ProtoBuf;

    [ProtoContract]
    public class SetCommandData : IPanelData
    {
        [ProtoMember(1)]
        public string Page { get; set; }
    }
}
