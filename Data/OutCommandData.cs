﻿

namespace PSPanel
{
    using ProtoBuf;

    [ProtoContract]
    public class OutCommandData : IPanelData
    {
        [ProtoMember(1)]
        public string Page { get; set; }

        [ProtoMember(2)]
        public int Row { get; set; }

        [ProtoMember(3)]
        public bool AnchorBottom { get; set; }

        [ProtoMember(4)]
        public string Value { get; set; }

        [ProtoMember(5)]
        public bool SkipFocus { get; set; }        
    }
}
