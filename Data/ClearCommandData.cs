﻿

namespace PSPanel
{
    using ProtoBuf;

    [ProtoContract]
    public class ClearCommandData : IPanelData
    {
        [ProtoMember(1)]
        public string Page { get; set; }
    }
}
